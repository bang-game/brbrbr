import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  final String helloText;

  MyHomePage({Key key, this.helloText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(helloText),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Любопытный странник?'),

//            TextField()
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          showDialog(
            context: context,
            child: AlertDialog(
              title: Text("Plus"),
              content: Text("Wow"),
            ),
            barrierDismissible: false,
          );
        },
      ),
    );
  }
}
