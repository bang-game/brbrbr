
import 'dart:io';

import 'dart:math';

main () {
  var a, b, c;
  print("Введите а:");
  a = int.parse(stdin.readLineSync());
  print("Введите b:");
  b = int.parse(stdin.readLineSync());
  print("Введите c:");
  c = int.parse(stdin.readLineSync());

  int d = b*b - 4 * a * c;
  if(d<0) print("Дискриминант отрицательный"+"\n");
  else {
    double x = ((-b)+sqrt(d))/(2*a);
    double y = ((-b)-sqrt(d))/(2*a);

    print("Дискриминант равен: $d " + "\n");
    print("x1: $x" + "\n");
    print("x2: $y" + "\n");
  }
}