import 'package:calculator/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('test show app', (WidgetTester tester) async {
    // отрисовка виджета
    await tester.pumpWidget(MaterialApp(
      home: MyHomePage(
        helloText: "Здравствуй",
      ),
    ));

    //проверка
    expect(find.text('Любопытный странник?'), findsOneWidget);
    expect(find.text('Я так и понял'), findsNothing);
  });
}
